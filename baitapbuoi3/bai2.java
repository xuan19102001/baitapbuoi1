package btvn_buoi3;

public class bai2 {
    private float x;
    private float y;
    private float xDelta;
    private float yDelta;
    private int radius;

    private bai2 (float x, float y,int radius, float xDelta, float yDelta) {
        this.setX(x);
        this.setY(y);
        this.setRadius(radius);
        this.setxDelta(xDelta);
        this.setyDelta(yDelta);
    }

    public static void main(String[] args) {
        System.out.println(new bai2(1.1f,2.2f,3,3.3f,4.4f));
        bai2 ball = new bai2(80.0f,35.0f,5,4.0f,6.0f);
        System.out.println("x is " +ball.getX() + "" +
                "\ny is " + ball.getY() +
                "\nradius is " + ball.getRadius() +
                "\nxDelta is " + ball.getxDelta() +
                "\nyDelta is " + ball.getyDelta());
        for (int i = 0; i < 2; i++) {
            ball.move(ball.getxDelta(), ball.getyDelta());
        }
        ball.reflectVertical();
        for (int i = 0; i < 2; i++) {
            ball.move(ball.getxDelta(), ball.getyDelta());
        }
        ball.reflectHorizontal();
        for (int i = 0; i < 6; i++) {
            ball.move(ball.getxDelta(), ball.getyDelta());
        }
        ball.reflectVertical();
        for (int i = 0; i < 5; i++) {
            ball.move(ball.getxDelta(), ball.getyDelta());
        }
    }

    private void setX(float x) {this.x = x;}

    private void setY(float y) {this.y = y;}

    private void setxDelta(float xDelta) {this.xDelta = xDelta;}

    private void setyDelta(float yDelta) {this.yDelta = yDelta;}

    private void setRadius(int radius) {this.radius = radius;}

    public float getX() {return this.x;}

    public float getY() {return this.y;}

    public float getxDelta() {return this.xDelta;}

    public float getyDelta() {return this.yDelta;}

    public int getRadius() {return this.radius;}

    private void move(float xDelta, float yDelta) {
        setX(getX() + xDelta);
        setY(getY() + yDelta);
        System.out.println(this);
    }

    private void reflectHorizontal() {
        this.setxDelta(-xDelta);
    }

    private void reflectVertical() {
        this.setyDelta(-yDelta);
    }

    public String toString() {
        String ball = "Ball[(%s,%s),speed=(%s,%s)]";
        return String.format(ball,this.getX(),this.getY(),this.getxDelta(),this.getyDelta());
    }
}
