package btvn_buoi3;

public class bai5 {
    private String name;
    private String address;

    public bai5 (String name, String address) {
        this.name = name;
        setAddress(address);
    }

    public static void main(String[] args) {
        bai5 person = new bai5("Nguyen Truong Xuan","Vinh Phuc");
        Student student = new Student(person.getName(),person.getAddress(),"Engineering",4,2);
        System.out.println(student);
        Staff staff = new Staff(person.getName(),person.getAddress(),"School",1);
        System.out.println(staff);
    }
    public String getName() {
        return name;
    }

    public String getAddress() {
        return address;
    }

    private void setAddress(String address) {
        this.address = address;
    }

    public String toString() {
        String person = "Person[name=%s, address=%s]";
        return String.format(person,getName(),getAddress());
    }

    static class Student extends bai5 {
        private String program;
        private int year;
        private double fee;

        public Student(String name, String address, String program, int year, double fee) {
            super(name, address);
            setYear(year);
            setProgram(program);
            setFee(fee);
        }

        public String getProgram() {
            return program;
        }

        public int getYear() {
            return year;
        }

        public double getFee() {
            return fee;
        }

        public void setProgram(String program) {
            this.program = program;
        }

        public void setYear(int year) {
            if (year >0) {
                this.year = year;
            }
        }

        public void setFee(double fee) {
            if (fee > 0) {
                this.fee = fee;
            }
        }

        public String toString() {
            String student = "Student %s,program=%s,year=%d,fee=%s";
            return String.format(student,super.toString(),getProgram(),getYear(),getFee());
        }
    }

    static class Staff extends bai5 {
        private String school;
        private double pay;

        public Staff(String name, String address,String school,double pay) {
            super(name, address);
            setSchool(school);
            setPay(pay);
        }

        public String getSchool() {
            return school;
        }

        public double getPay() {
            return pay;
        }

        public void setSchool(String school) {
            this.school = school;
        }

        public void setPay(double pay) {
            if (pay > 0) {
                this.pay = pay;
            }
        }

        public String toString() {
            String staff = "Staff %s,school=%s,pay=%s";
            return String.format(staff,super.toString(),getSchool(),getPay());
        }
    }
}
