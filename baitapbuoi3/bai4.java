package btvn_buoi3;

public class bai4 {
    private int id;
    private String name;
    private int discount;

    public bai4(int id, String name, int discount) {
        this.id = id;
        this.name = name;
        this.setDiscount(discount);
    }

    public static void main(String[] args) {
        bai4 customer = new bai4(88,"Tan Ah Teck",10);
        System.out.println(customer);
        bai4 customer1 = new bai4(88,"Tan Ah Teck",8);
        System.out.println("id is: " + customer1.getId() + "\n" +
                "name is: " + customer1.getName() + "\n" +
                "discount is: " + customer1.getDiscount());
        Invoice invoice = new Invoice(101,customer1,888.8);
        System.out.println(invoice);
        Invoice invoice1 = new Invoice(101,customer1,999.9);
        System.out.println(invoice);
        System.out.println("id is: " + invoice1.getId() + "\n" +
                "customer is: " + invoice1.getCustom() + "\n" +
                "amount is: " + invoice1.getAmount() + "\n" +
                "customer's id is: " + invoice1.getCustomerId() + "\n" +
                "customer's name is: " + invoice1.getCustomerName() + "\n" +
                "customer's discoint is: " + invoice1.getCustomerDiscount());
        System.out.println("Amount after discount is: " + invoice1.getAmountAfterDiscount());
    }

    public int getId() {return this.id;}
    public String getName() {return this.name;}
    public int getDiscount() {return this.discount;}

    private void setDiscount(int discount) {
        if (discount > 100) {
            discount = 100;
        } else if (discount < 0) {
            discount = 0;
        }
        this.discount = discount;
    }

    public String toString() {
        String customer = getName() +"(" + getId() + ")(" + getDiscount() + "%)";
        return customer;
    }

    public static class Invoice{
        private int id;
        bai4 customer;
        private double amount;

        public Invoice(int id,bai4 customer,double amount) {
            this.id = id;
            this.setCustomer(customer);
            this.setAmount(amount);
        }

        public int getId() {return this.id;}
        public bai4 getCustom() {return this.customer;}
        public double getAmount() {return this.amount;}
        public int getCustomerId() {return this.customer.getId();}
        public String getCustomerName() {return  this.customer.getName();}
        public int getCustomerDiscount() {return this.customer.getDiscount();}
        public double getAmountAfterDiscount() {return (this.getAmount())*(1-this.getCustomerDiscount()/100d);}

        private void setCustomer(bai4 customer) {
            this.customer = customer;
        }

        public void setAmount(double amount) {
            this.amount = amount;
        }

        public String toString() {
            String invoice = "Invoice[id=%d,customer=%s(%d)(%d),amount=%s";
            return String.format(invoice,getId(),getCustomerName(),getCustomerId(),getCustomerDiscount(),getAmount());
        }
    }
}
