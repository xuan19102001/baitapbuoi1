package btvn_buoi3;

import java.time.LocalTime;

public class bai1 {
    private int hour,minute,second;

    private bai1(int hour, int minute, int second) {
        this.setHour(hour);
        this.setMinute(minute);
        this.setSecond(second);
    }

    private void setHour(int hour){
        if (hour <= 23 && hour >= 0) {
            this.hour = hour;
        }
    }

    private void setMinute(int minute){
        if (minute <= 59 && minute >= 0) {
            this.minute = minute;
        }
    }

    private void setSecond(int second){
        if (second <= 59 && second >= 0) {
            this.second = second;
        }
    }

    private int getHour(){
        return this.hour;
    }

    private int getMinute(){return this.minute;}

    private int getSecond(){return this.second;}

    private void nextSecond() {
        if (this.getSecond() == 59) {
            this.setSecond(0);
            if (this.getMinute() ==  59) {
                this.setMinute(0);
                if (this.getHour() == 23) {
                    this.setHour(0);
                } else {
                    this.setHour(this.hour+1);
                }
            } else {
                this.setMinute(this.minute+1);
            }
        } else {
            this.setSecond(this.second+1);
        }
        System.out.println(this);
    }

    private void previusSecond() {
        setSecond(getSecond()+1);
        System.out.println(this);
    }

    public String toString() {
        String time = "";
        if (this.hour > 23 || this.minute > 59
                || this.getSecond() > 59 || this.getHour() < 0
                || this.getMinute() < 0 || this.getSecond() < 0) {
            time = "ERROR FORMAT!";
        } else {
            LocalTime t = LocalTime.of(this.getHour(), this.getMinute(), this.getSecond());
            time = "Time " + t ;
        }
        return time;
    }
    public static void main(String[] args) {
        System.out.println(new bai1(1,2,3));
        bai1 time = new bai1(4,5,6);
        System.out.println(time);
        System.out.println("Hour " + time.getHour() + "\n" +
                "Minute " + time.getMinute() + "\n" +
                "Second " + time.getSecond());
        bai1 time1 = new bai1(23,59,58);
        System.out.println(time1);
        time1.nextSecond();
        time1.nextSecond();
        time1.nextSecond();
        time1.previusSecond();
        time1.previusSecond();
    }
}
