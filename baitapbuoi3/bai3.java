package btvn_buoi3;

public class bai3 {
    private String name;
    private String email;
    private Character gender;

    public bai3 (String name, String email, Character gender) {
        this.name = name;
        this.setEmail(email);
        this.gender =gender;
    }


    private void setEmail(String email) {this.email = email;}

    public String getName() {return this.name;}
    public String getEmail() {return this.email;}
    public Character getGender() {
        if (!gender.equals('m') && !gender.equals('f')) {
            return ' ';
        }
        return this.gender;
    }

    public String toString() {
        String author = "Author[name=%s,email=%s,gender=%s]";
        return String.format(author,this.getName(),this.getEmail(),this.getGender());
    }

    public static class Book {
        private String name;
        bai3 auth;
        private double price;
        private int quantity = 0;

        public Book(String name,bai3 author,Double price) {
            this.name = name;
            this.auth = author;
            this.setPrice(price);
        }

        public Book(String name,bai3 author,Double price, int quantity) {
            this.name = name;
            this.auth = author;
            this.setPrice(price);
            this.setQuantity(quantity);
        }

        private void setPrice(Double price) {
            if (price > 0) {
                this.price = price;
            }
        }
        private void setQuantity(int quantity) {
            if (quantity > 0) {
                this.quantity = quantity;
            }
        }

        public String getName() {return this.name;}
        public bai3 getAuth() {return this.auth;}
        public Double getPrice() {return this.price;}
        public int getQuantity() {return this.quantity;}

        public String toString() {
            String book = "Book[name=%s,%s,price=%s,quantity=%d]";
            return String.format(book,this.getName(),this.getAuth(),this.getPrice(),this.getQuantity());
        }

    }
    public static void main(String[] args) {
        bai3 auThor = new bai3("Nguyen Truong Xuan","xnt@gmail.com",'m');
        Book book = new Book("Hello",auThor,50d,1);
        System.out.println(book);
    }
}
